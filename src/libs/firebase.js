import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

require("dotenv").config();

initializeApp({
  apiKey: "AIzaSyCT9g_-kCg32Jmi2dtnPoGwn7WLox-Wra0",
  authDomain: "todolist-96a5b.firebaseapp.com",
  projectId: "todolist-96a5b",
  storageBucket: "todolist-96a5b.appspot.com",
  messagingSenderId: "356538726597",
  appId: "1:356538726597:web:c6aedcc2f1e393104cd8ac",
});

const db = getFirestore();

export default db;
