import React, { useState } from "react";
import styled, { css } from "styled-components";
import { Checkbox, IconButton, TextField } from "@mui/material";

import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

import db from "../libs/firebase";
import { doc, setDoc, deleteDoc } from "firebase/firestore";

const Dont = css`
  background-color: #e5dcc3;
`;

const Do = css`
  display: block;
  filter: blur(2px);
  opacity: 0.95;
`;

const CardContainer = styled.div`
  display: flex;
  justify-content: flext-start;
  align-content: center;
  align-items: center;
  width: 25vw;
  padding: 10px;
  border-radius: 10px;
  margin: 10px;
  background-color: #e5dcc3;

  ${({ checked }) => (checked === false ? Dont : Do)}
`;

export default function TodoCard({ items }) {
  //   console.log(items);
  const [changeToTextField, setChangeToTextField] = useState(false);
  const [checked, setChecked] = useState(items.finished);
  const [text, setText] = useState("");

  const handleEdit = () => {
    setChangeToTextField(!changeToTextField);

    if (items.todo === text || text === "") {
      //   console.log("Not change");
    } else {
      //   console.log("change");

      const timestamp = new Date();
      const docRef = doc(db, "todoList-db", items.id);
      setDoc(docRef, {
        timestamp: timestamp,
        todo: text,
        finished: checked,
      });
    }
  };

  const changeText = (e) => {
    e.preventDefault();

    setText(e.target.value);
  };

  const handleClick = async () => {
    const docRef = doc(db, "todoList-db", items.id);
    await deleteDoc(docRef);
  };

  const onChangeCheckBox = async (e) => {
    setChecked(e.target.checked);
    const timestamp = new Date();
    const docRef = doc(db, "todoList-db", items.id);

    if (e.target.checked === true) {
      await setDoc(docRef, {
        timestamp: timestamp,
        todo: items.todo,
        finished: true,
      });
    } else {
      await setDoc(docRef, {
        timestamp: timestamp,
        todo: items.todo,
        finished: false,
      });
    }
  };

  return (
    <CardContainer checked={checked}>
      {changeToTextField ? null : (
        <Checkbox
          inputProps={{ "aria-label": "controlled" }}
          checked={checked}
          onChange={onChangeCheckBox}
        />
      )}

      {changeToTextField ? (
        <TextField
          required
          id="outlined-required"
          label="Todo"
          size="small"
          value={text}
          onChange={changeText}
        />
      ) : (
        <span style={{ overflow: "hidden", textOverflow: "ellipsis" }}>
          {items.todo}
        </span>
      )}

      {checked ? null : (
        <div>
          <IconButton aria-label="edit" onClick={handleEdit}>
            <EditIcon />
          </IconButton>
          <IconButton aria-label="delete" onClick={handleClick} color="error">
            <DeleteIcon />
          </IconButton>
        </div>
      )}
    </CardContainer>
  );
}
