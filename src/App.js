import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Button, TextField, Typography, Box, Paper } from "@mui/material";
import SendIcon from "@mui/icons-material/Send";

import TodoCard from "./components/TodoCard";

import db from "./libs/firebase";
import { onSnapshot, collection, addDoc } from "firebase/firestore";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #9a9483;
  width: 100vw;
  min-height: 100vh;
  padding: 30px;
  
`;

const TextFieldContainer = styled.div`
  display: flex;
  align-items: baseline;
`;

const PaperTodo = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #;
`;

export default function App() {
  const [todo, setTodo] = useState([]);

  const [todoName, setTodoName] = useState("");

  const handleText = (e) => {
    e.preventDefault();

    setTodoName(e.target.value);

    // console.log(todo);
  };
  const onAddTodo = async () => {
    const collectionRef = collection(db, "todoList-db");
    const timestamp = new Date();

    await addDoc(collectionRef, {
      timestamp: timestamp,
      finished: false,
      todo: todoName,
    });
    // console.log("The new ID is: " + docRef.id);
    setTodoName("");
  };

  useEffect(
    () =>
      onSnapshot(collection(db, "todoList-db"), (snapshot) =>
        setTodo(snapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id })))
      ),
    []
  );

  useEffect(() => {
    // console.log(todoName);
    // console.log(todo);
  }, [todo, todoName]);

  return (
    <Container>
      <Paper
        variant="outlined"
        style={{ padding: "50px", borderRadius: "20px" }}
      >
        <Typography variant="h1" component="h2">
          Todo-list
        </Typography>

        <Box
          component="form"
          sx={{
            "& .MuiTextField-root": { m: 1, width: "50ch" },
          }}
          noValidate
          autoComplete="off"
        >
          <TextFieldContainer>
            <TextField
              required
              id="outlined-required"
              label="Todo"
              helperText="Please enter your todo"
              size="small"
              value={todoName}
              onChange={handleText}
            />
            <Button
              variant="contained"
              size="medium"
              endIcon={<SendIcon />}
              onClick={onAddTodo}
            >
              Add
            </Button>
          </TextFieldContainer>
        </Box>
      </Paper>

      <PaperTodo>
        {todo
          .sort(function (a, b) {
            return b.timestamp - a.timestamp;
          })
          .map((item) => (
            <TodoCard key={item.id} items={item} />
          ))}
      </PaperTodo>
    </Container>
  );
}
